#!groovy

node {
      // Get the Maven tool.
      // ** NOTE: This 'default_maven' Maven tool must be configured
      // **       in the global configuration. 
   def projectName = "airinu-gcs-uploader"
   def git_branch = "develop"                // product
   def deploy_host = "35.187.238.228"     // product
   def deploy_suffix = "develop"             // product - suffix for files like run-dev.sh, application-dev.properties
   def deploy_include = ""    // product          
   // def projectName = "airinu-app-api"
   // def git_branch = "test"                // test
   // def deploy_host = "103.63.109.153"     // test
   // def deploy_suffix = "test"             // test - suffix for files like run-dev.sh, application-dev.properties
   // def deploy_exclude = "config/*dev*"    // test
   // def git_branch = "develop"          // dev
   // def deploy_host = "35.199.156.249"  // dev
   // def deploy_suffix = "dev"           // dev - suffix for files like run-dev.sh, application-dev.properties
   // def deploy_exclude = "config/*test*"// dev

   def deploy_username = "airinu"
   def deploy_tempFolder = "/tmp/airinu/app"
   def deploy_mainFolder = "/home/airinu/app"
   def deploy_remotePort = "22"

   def git_rootUrl = "ssh://gogs@git.dtsgroup.com.vn:2222/Airinu_1.0_YourTV"
   def git_credentialsId = "gianglt_np_rsa_4096"
   def mvnHome = tool "default_maven"
   
   try {
       notifyBuild('STARTED')
       stage('Preparation') { // for display purposes
          // Get some code from a GitHub repository
          git url: "${git_rootUrl}/${projectName}.git", branch: "${git_branch}", credentialsId: "${git_credentialsId}"
       }
        
       stage('Build') {
          // Run the maven build
          if (isUnix()) {
             sh "'${mvnHome}/bin/mvn' -Dmaven.test.failure.ignore clean package"
          } else {
             bat(/"${mvnHome}\bin\mvn" -Dmaven.test.failure.ignore clean package/)
          }
          // junit '**/target/surefire-reports/TEST-*.xml'
          archive 'target/*.jar'
       }
       
//       stage ('Code analyse') {
//            sh 'echo "Run some lints"'
//            sh "'${mvnHome}/bin/mvn' sonar:sonar -Dsonar.host.url=http://sonar.dtsgroup.com.vn -Dsonar.login=60e02c9b7e197be9c08527c3f102b358d2bd6aaa"
//       }

        stage ('Unit test') {
          sh 'echo "Tests will back"'
        }

        stage ('Deploy') {
          sh "ssh -p ${deploy_remotePort} -oStrictHostKeyChecking=no ${deploy_username}@${deploy_host} rm -rf   ${deploy_tempFolder}/${projectName}"
          sh "ssh -p ${deploy_remotePort} -oStrictHostKeyChecking=no ${deploy_username}@${deploy_host} mkdir -p ${deploy_tempFolder}/${projectName}/config"
          sh "cd ${workspace}"
          sh "scp -P ${deploy_remotePort} -r target/*.jar run-${deploy_suffix}.sh ${deploy_username}@${deploy_host}:${deploy_tempFolder}/${projectName}"
          sh "scp -P ${deploy_remotePort} -r config/log4j2-${deploy_suffix}.xml config/application-${deploy_suffix}.yml ${deploy_username}@${deploy_host}:${deploy_tempFolder}/${projectName}/config"
          sh "ssh -p ${deploy_remotePort} -oStrictHostKeyChecking=no ${deploy_username}@${deploy_host} 'mkdir -p  ${deploy_mainFolder}/${projectName}'"
          sh "ssh -p ${deploy_remotePort} -oStrictHostKeyChecking=no ${deploy_username}@${deploy_host} 'rm -rf  ${deploy_mainFolder}/${projectName} && mv ${deploy_tempFolder}/${projectName} ${deploy_mainFolder}/${projectName}'"
          sh "ssh -p ${deploy_remotePort} -oStrictHostKeyChecking=no ${deploy_username}@${deploy_host} chmod +x ${deploy_mainFolder}/${projectName}/*.sh"
          sh "ssh -p ${deploy_remotePort} -oStrictHostKeyChecking=no ${deploy_username}@${deploy_host} ${deploy_mainFolder}/${projectName}/run-${deploy_suffix}.sh restart"
        }
   } catch (e) {
    // If there was an exception thrown, the build failed
    currentBuild.result = "FAILED"
    throw e
  } finally {
    // Success or failure, always send notifications
    notifyBuild(currentBuild.result)
  }

}


def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"
  def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary)

  // emailext(
  //     subject: subject,
  //     body: details,
  //     recipientProviders: [[$class: 'DevelopersRecipientProvider']]
  //   )
}

package com.dts.airinu.gcsuploader;

import com.dts.airinu.communication.common.app.AppConstant;
import com.dts.airinu.gcsuploader.config.AirinuApiConfig;
import com.dts.airinu.gcsuploader.config.AirinuConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rin
 */
@SpringBootApplication
public class GCSApplication {
    public static void main(String[] args) {
        SpringApplication.run(GCSApplication.class, args);
        AirinuApiConfig.configuration(AirinuConstant.CONFIG_FILE_NAME, AirinuConstant.CONFIG_FOLDER);
    }
}

package com.dts.airinu.gcsuploader;

import com.dts.airinu.communication.rabbit.receiver.MessageReceivedHandler;
import com.dts.airinu.gcsuploader.receiver.UploadFileReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReceiverConfiguration {
    private final Logger logger = LoggerFactory.getLogger(ReceiverConfiguration.class);

    private final UploadFileReceiver uploadFileReceiver;

    @Autowired
    public ReceiverConfiguration(UploadFileReceiver uploadFileReceiver) {
        this.uploadFileReceiver = uploadFileReceiver;
    }

    @Bean
    MessageReceivedHandler createHandler() {
        MessageReceivedHandler handler = new MessageReceivedHandler();
        try {
            handler.addReceiver(uploadFileReceiver);
            logger.info("start receiver");
        } catch (Exception e) {
            logger.error("e", e);
        }
        return handler;
    }

}

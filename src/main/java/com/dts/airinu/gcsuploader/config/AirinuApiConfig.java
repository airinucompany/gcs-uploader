package com.dts.airinu.gcsuploader.config;


import org.jconfig.Configuration;

import java.io.File;

public class AirinuApiConfig {

    private static String configName;
    private static boolean configured = false;

    public static void configuration(String name, String configPath) {
        File f = new File(configPath);
        if (!f.exists()) {
            f.mkdirs();
        }
        if (!name.endsWith("xml")) {
            configPath += name + ".xml";
            configName = name;
        } else {
            configPath += name;
            configName = name.replace(".xml", "");
        }
        File fileGameConfig = new File(configPath);
        ConfigApiManager.addConfig(configName, fileGameConfig, AirinuConstant.DEFAULT_CONTENT);
        configured = true;
    }

    public static boolean isConfigured() {
        return configured;
    }

    public static Configuration getConfig() {
        return ConfigApiManager.getConfig(configName);
    }

}

package com.dts.airinu.gcsuploader.config;

import com.dts.airinu.communication.common.app.AppConstant;

import java.io.File;

public class AirinuConstant {

    public static final String CONFIG_FILE_NAME = "gcs-uploader.xml";
    public static final String CONFIG_FOLDER = AppConstant.BASE_FOLDER + "config" + java.io.File.separator + "gcs-uploader" + java.io.File.separator;

    public static String DEFAULT_CONTENT;

    static {
        DEFAULT_CONTENT = "<?xml version=\"1.0\" ?>\n" +
                "<properties>\n" +
                "    <category name=\"SETTING\">\n" +
                "        <property name=\"CREDENTIAL_FILE\" value=\"production-airinu-97e0c8b47ac5.json\"/>\n" +
                "        <property name=\"BUCKET_NAME\" value=\"unverified-videos\"/>\n" +
                "        <property name=\"HOST\" value=\"https://streaming.airinu.com\"/>\n" +
                "        <property name=\"VOD\" value=\"airinua-streaming-application\"/>\n" +
                "    </category>\n" +
                "</properties>\n";
    }
}

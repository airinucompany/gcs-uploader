package com.dts.airinu.gcsuploader.config;

import org.jconfig.Configuration;
import org.jconfig.ConfigurationManager;
import org.jconfig.ConfigurationManagerException;
import org.jconfig.handler.XMLFileHandler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class ConfigApiManager {

    public static synchronized void addConfig(String configName, File file,
                                              String defaultContent) {
        ConfigurationManager cm = ConfigurationManager.getInstance();
        try {
            if (!file.exists()) {
                createDefaultConfigFile(file, defaultContent);
            }
            XMLFileHandler fileHandler = new XMLFileHandler();
            fileHandler.setFile(file);
            cm.load(fileHandler, configName);
        } catch (ConfigurationManagerException cme1) {
            cme1.printStackTrace();
        }
    }


    public static Configuration getConfig(String configName) {
        Configuration config;
        config = ConfigurationManager.getConfiguration(configName);
        return config;
    }

    private static void createDefaultConfigFile(File file, String defaultContent) {
        BufferedWriter w;
        try {
            w = new BufferedWriter(new FileWriter(file));
            w.write(defaultContent);
            w.flush();
            w.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

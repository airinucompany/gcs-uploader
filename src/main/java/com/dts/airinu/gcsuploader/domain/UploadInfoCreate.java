package com.dts.airinu.gcsuploader.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Rin
 */
@Getter
@Setter
@ToString
public class UploadInfoCreate {
    private String fileName;
    private String fileVideo;
    private String fileGif;
    private String fileVideoOrigin;
    private String[] linkVideo;
    private int typeUpload; // 0. video, 1. GIF
    private String createdBy;

    private int duration;
    private int width;
    private int height;
}

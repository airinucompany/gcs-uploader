package com.dts.airinu.gcsuploader.domain;

        import com.google.cloud.storage.Blob;
        import lombok.Getter;
        import lombok.Setter;
        import lombok.ToString;

/**
 * @author Rin
 */
@Setter
@Getter
@ToString
public class UploadSuccessInfo {
    private Blob blob;
    private Blob blobImg;
    private String signUrl;
    private String signUrlTrim;
    private String mediaUrl;
    private String imgUrl;
}

package com.dts.airinu.gcsuploader.receiver;

import com.dts.airinu.communication.domain.bean.Response;
import com.dts.airinu.communication.domain.document.BaseDocument;
import com.dts.airinu.communication.domain.enumeration.Action;
import com.dts.airinu.communication.rabbit.receiver.RabbitReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Rin
 */
public abstract class BaseReceiver<T extends BaseDocument> extends RabbitReceiver<T> {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Response doMessageSync(T message, String messageId, Action action) {
        logger.info("doMessageSync--{}", message);
        throw new UnsupportedOperationException();
    }
}

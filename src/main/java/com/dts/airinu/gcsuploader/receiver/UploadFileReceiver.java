package com.dts.airinu.gcsuploader.receiver;

import com.dts.airinu.communication.ant.QueueReceiver;
import com.dts.airinu.communication.domain.document.storeage.UploadInfo;
import com.dts.airinu.communication.domain.document.storeage.upload.GcsUploadInfo;
import com.dts.airinu.communication.domain.enumeration.Action;
import com.dts.airinu.gcsuploader.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Rin
 */
@Component
@QueueReceiver(queueName = "[airinu-gcs-uploader]gcs-upload-data")
public class UploadFileReceiver extends BaseReceiver<GcsUploadInfo> {
    @Autowired

    private UploadService uploadService;

    @Override
    public void doMessageASync(GcsUploadInfo message, Action action) {
        logger.info("=>>>doMessageASync--{}", message);
        try {
            uploadService.excute(message);
        }
        catch (Exception e)
        {
            logger.error("Err",e);
        }
    }

}

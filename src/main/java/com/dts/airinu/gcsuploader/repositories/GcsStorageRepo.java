package com.dts.airinu.gcsuploader.repositories;

import com.dts.airinu.communication.domain.document.storeage.upload.GcsStorage;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Rin
 */
public interface GcsStorageRepo  extends MongoRepository<GcsStorage,String> {
}

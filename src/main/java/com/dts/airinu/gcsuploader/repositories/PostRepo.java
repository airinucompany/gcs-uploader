package com.dts.airinu.gcsuploader.repositories;

import com.dts.airinu.communication.domain.document.post.Post;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Rin
 */
public interface PostRepo extends MongoRepository<Post,String> {
}

package com.dts.airinu.gcsuploader.repositories;

import com.dts.airinu.communication.domain.document.storeage.UploadInfo;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Rin
 */
public interface UploadInfoRepo extends MongoRepository<UploadInfo, String> {
}

package com.dts.airinu.gcsuploader.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Rin
 */
public abstract class BaseService {
    protected final Logger logger= LoggerFactory.getLogger(this.getClass());
}

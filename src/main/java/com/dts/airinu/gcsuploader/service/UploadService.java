package com.dts.airinu.gcsuploader.service;

import com.dts.airinu.communication.common.RecordStatus;
import com.dts.airinu.communication.common.app.AppConstant;
import com.dts.airinu.communication.domain.document.post.Post;
import com.dts.airinu.communication.domain.document.storeage.PostStreamUrl;
import com.dts.airinu.communication.domain.document.storeage.PostThumb;
import com.dts.airinu.communication.domain.document.storeage.UploadInfo;
import com.dts.airinu.communication.domain.document.storeage.upload.GcsBlobId;
import com.dts.airinu.communication.domain.document.storeage.upload.GcsStorage;
import com.dts.airinu.communication.domain.document.storeage.upload.GcsUploadInfo;
import com.dts.airinu.gcsuploader.config.AirinuApiConfig;
import com.dts.airinu.gcsuploader.domain.UploadSuccessInfo;
import com.dts.airinu.gcsuploader.repositories.GcsStorageRepo;
import com.dts.airinu.gcsuploader.repositories.PostRepo;
import com.dts.airinu.gcsuploader.repositories.UploadInfoRepo;
import com.dts.airinu.gcsuploader.utils.GCSConnection;
import com.dts.airinu.gcsuploader.utils.GcsDefs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;

/**
 * @author Rin
 */
@Service
public class UploadService extends BaseService {

    private final UploadInfoRepo uploadInfoRepo;
    private final PostRepo postRepo;
    private final GcsStorageRepo gcsStorageRepo;

    @Autowired
    public UploadService(PostRepo postRepo, UploadInfoRepo uploadInfoRepo, GcsStorageRepo gcsStorageRepo) {
        this.postRepo = postRepo;
        this.uploadInfoRepo = uploadInfoRepo;
        this.gcsStorageRepo = gcsStorageRepo;
    }
//    structure sẽ là: file_name.mp4 và file_name/screen_shot_001.jpeg.
//    Cái ffmpeg cmd là  ffmpeg -i input.flv -vf fps=1/60 input/screen_shot_%03d.jpeg.

    private int deleteFile(String pathName) {
        try {
            Path path = FileSystems.getDefault().getPath(pathName);
            Files.delete(path);
            return 0;
        } catch (IOException ie) {
            logger.error("IOException: ", ie);
        }
        return -1;
    }


    public void uploadImage(UploadInfo uploadInfo) {
        logger.info("====>********* [Upload image] info detail =>>> {}", uploadInfo);
        if (!new File(uploadInfo.getFileGif()).exists()) {
            logger.info("File not exist . Upload failed ..*********");
            return;
        }
        try {
            Path path = Paths.get(uploadInfo.getFileGif());
            String contentType = GcsDefs.MIME_TYPE_IMAGE_JPEG;
            if (uploadInfo.getFileGif().endsWith(".png"))
                contentType = GcsDefs.MIME_TYPE_IMAGE_PNG;
            String BUCKET_NAME = AirinuApiConfig.getConfig().getProperty("BUCKET_NAME", "unverified-videos", "SETTINGS");
            Future<UploadSuccessInfo> future = GCSConnection.getInstance().uploadImage(path, uploadInfo.getFileName(), BUCKET_NAME, contentType);
            if (future == null) {
                logger.info("Upload to Google cloud storage server failed");
            } else {
                UploadSuccessInfo info = future.get();
                if (info != null) {
                    logger.info("Upload image to Google cloud  success:: {}", info);

                    deleteFile(uploadInfo.getFileGif());

                    logger.info("====********* Upload info update =>>> {}", uploadInfo);
                    uploadInfo.setStatus(AppConstant.UPLOAD_STATUS_SUCCESS);
                    uploadInfoRepo.save(uploadInfo);
                    String postId = uploadInfo.getPostId();
                    Optional<Post> optionalPost = postRepo.findById(postId);
                    logger.info("=>*********Upload post url : ");
                    if (optionalPost.isPresent()) {
                        Post post = optionalPost.get();
                        PostThumb thumb = new PostThumb();
                        thumb.setType("default");
                        thumb.setUrl(info.getImgUrl());
                        thumb.setWidth(640);
                        thumb.setHeight(480);
                        post.getThumb().add(thumb);
                        logger.info("=*********Upload thumb post :: {}", post);
                        postRepo.save(post);

                        GcsStorage gcsStorage = new GcsStorage();
                        GcsBlobId blobId = new GcsBlobId();
                        blobId.setBucket(info.getBlobImg().getBucket());
                        blobId.setGeneration(info.getBlobImg().getGeneration());
                        blobId.setName(info.getBlobImg().getName());

                        gcsStorage.setBlobId(blobId);
                        gcsStorage.setBucket(info.getBlobImg().getBucket());
                        gcsStorage.setContentType(info.getBlobImg().getContentType());
                        gcsStorage.setCreateTime(new Date(info.getBlobImg().getCreateTime()));
                        gcsStorage.setUpdateTime(new Date(info.getBlobImg().getUpdateTime()));
                        gcsStorage.setType(GcsStorage.Type.IMAGE.getType());
                        gcsStorage.setMediaLink(info.getMediaUrl());
                        gcsStorage.setSelfLink(info.getBlobImg().getSelfLink());
                        gcsStorage.setSignUrl(info.getImgUrl());
                        gcsStorage.setPostId(post.getId());
                        gcsStorageRepo.save(gcsStorage);
                    } else {
                        logger.info("Post id: {}  not found  ", postId);
                    }
                } else {
                    logger.info("[Upload image] to Google cloud storage server failed");
                }
            }
        } catch (Exception e) {
            logger.error("Err", e);
        }
    }

    public void uploadVideo(UploadInfo uploadInfo) {
        logger.info("====>>>>>>> Upload  video info detail =>>> {}", uploadInfo);
        if (!new File(uploadInfo.getFileVideo()).exists()) {
            logger.info("File not exist . Upload failed");
            return;
        }
        try {
            String BUCKET_NAME = AirinuApiConfig.getConfig().getProperty("BUCKET_NAME", "unverified-videos", "SETTINGS");
            Path path = Paths.get(uploadInfo.getFileVideo());
            Future<UploadSuccessInfo> future = GCSConnection.getInstance().uploadVideo(path, uploadInfo.getFileName(), BUCKET_NAME, GcsDefs.MIME_TYPE_VIDEO_MP4);
            if (future == null) {
                logger.info("Upload to Google cloud storage server failed");
            } else {

                UploadSuccessInfo info = future.get();
                if (info != null) {
                    logger.info("Upload to Google cloud  success:: {}", info);

                    deleteFile(uploadInfo.getFileVideo());

                    logger.info("====>>>>>>> Upload info update =>>> {}", uploadInfo);
                    uploadInfo.setStatus(AppConstant.UPLOAD_STATUS_SUCCESS);
                    uploadInfoRepo.save(uploadInfo);
                    String postId = uploadInfo.getPostId();
                    Optional<Post> optionalPost = postRepo.findById(postId);
                    logger.info("=>>>Upload post url : ");
                    if (optionalPost.isPresent()) {
                        Post post = optionalPost.get();
                        PostThumb thumb = new PostThumb();
                        thumb.setType("default");
                        thumb.setUrl(info.getImgUrl());
                        thumb.setWidth(640);
                        thumb.setHeight(480);
                        post.getThumb().add(thumb);
                        logger.info("=>>>>>>Upload post :: {}", post);
                        post.setStatus(RecordStatus.WAITING_APPROVE);
                        post.setUrl(info.getSignUrlTrim());

                        //    35.247.179.252:1935/airinua-streaming-application/_definst_/mp4:https/unverified-videos/triettestvod.mp4/playlist.m3u8

                        List<PostStreamUrl> streamUrls = new ArrayList<>();
                        PostStreamUrl postStreamUrl = new PostStreamUrl();
                        String HOST = AirinuApiConfig.getConfig().getProperty("HOST", "https://streaming.airinu.com", "SETTINGS");
                        String VOD = AirinuApiConfig.getConfig().getProperty("VOD", "airinua-streaming-application", "SETTINGS");
                        String httpLink = HOST + "/" + VOD + "/_definst_/mp4:https/" + BUCKET_NAME + "/$FILE_NAME$/playlist.m3u8";
                        httpLink = httpLink.replaceAll("\\$FILE_NAME\\$", uploadInfo.getFileName());
                        postStreamUrl.setUrl(httpLink);
                        postStreamUrl.setQuality("default");
                        streamUrls.add(postStreamUrl);

                        postStreamUrl = new PostStreamUrl();
                        postStreamUrl.setUrl(info.getSignUrlTrim());
                        postStreamUrl.setQuality("default");
                        streamUrls.add(postStreamUrl);


                        post.setStreamUrl(streamUrls);

                        postRepo.save(post);

                        List<GcsStorage> storages = new ArrayList<>();

                        GcsStorage gcsStorage = new GcsStorage();
                        GcsBlobId blobId = new GcsBlobId();
                        blobId.setBucket(info.getBlob().getBucket());
                        blobId.setGeneration(info.getBlob().getGeneration());
                        blobId.setName(info.getBlob().getName());
                        gcsStorage.setBlobId(blobId);
                        gcsStorage.setBucket(info.getBlob().getBucket());
                        gcsStorage.setContentType(info.getBlob().getContentType());
                        gcsStorage.setCreateTime(new Date(info.getBlob().getCreateTime()));
                        gcsStorage.setUpdateTime(new Date(info.getBlob().getUpdateTime()));
                        gcsStorage.setType(GcsStorage.Type.MEDIA.getType());
                        gcsStorage.setMediaLink(info.getMediaUrl());
                        gcsStorage.setSelfLink(info.getBlob().getSelfLink());
                        gcsStorage.setSignUrl(info.getSignUrl());
                        gcsStorage.setPostId(post.getId());
                        storages.add(gcsStorage);

                        gcsStorage = new GcsStorage();
                        blobId = new GcsBlobId();
                        blobId.setBucket(info.getBlobImg().getBucket());
                        blobId.setGeneration(info.getBlobImg().getGeneration());
                        blobId.setName(info.getBlobImg().getName());
                        gcsStorage.setBlobId(blobId);
                        gcsStorage.setBucket(info.getBlobImg().getBucket());
                        gcsStorage.setContentType(info.getBlobImg().getContentType());
                        gcsStorage.setCreateTime(new Date(info.getBlobImg().getCreateTime()));
                        gcsStorage.setUpdateTime(new Date(info.getBlobImg().getUpdateTime()));
                        gcsStorage.setType(GcsStorage.Type.IMAGE.getType());
                        gcsStorage.setMediaLink(info.getBlobImg().getMediaLink());
                        gcsStorage.setSelfLink(info.getBlobImg().getSelfLink());
                        gcsStorage.setSignUrl(info.getImgUrl());
                        gcsStorage.setPostId(post.getId());
                        storages.add(gcsStorage);
                        gcsStorageRepo.saveAll(storages);

                    } else {
                        logger.info("Post id: {}  not found  ", postId);
                    }
                } else {
                    logger.info("Upload to Google cloud storage server failed");
                }
            }
        } catch (Exception e) {
            logger.error("Err", e);
            String postId = uploadInfo.getPostId();
            try {
                Optional<Post> optionalPost = postRepo.findById(postId);
                if (optionalPost.isPresent()) {
                    optionalPost.get().setStatus(RecordStatus.FAIL);
                    postRepo.save(optionalPost.get());
                }
                logger.info("=>>>Upload post url : ");
            } catch (Exception ex) {
                logger.error("Err", ex);
            }
        }
    }

    public void excute(GcsUploadInfo message) {
        logger.info("=>>Upload info : {}", message);
        String uId = message.getUploadId();
        Optional<UploadInfo> optional = uploadInfoRepo.findById(uId);
        if (optional.isPresent()) {
            UploadInfo uploadInfo = optional.get();
            if (uploadInfo.getTypeUpload() == AppConstant.TYPE_UPLOAD_GIF)
                uploadImage(uploadInfo);
            else if (uploadInfo.getTypeUpload() == AppConstant.TYPE_UPLOAD_VIDEO)
                uploadVideo(uploadInfo);
        } else {
            logger.info("Upload info fail not found info");
        }
    }
}

package com.dts.airinu.gcsuploader.utils;

import com.dts.airinu.gcsuploader.config.AirinuApiConfig;
import com.dts.airinu.gcsuploader.domain.UploadSuccessInfo;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.*;
import com.google.common.collect.Lists;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.*;


/**
 * @author Rin
 */
public class GCSConnection {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String BASE_CREDENTIAL_PATH = System.getProperty("user.dir") + "/config/";
    // public static final String BUCKET_NAME = "unverified-videos";
    public static final String JPEG_CONTENT_TYPE = "image/jpeg";
    private GoogleCredentials credentials;


    public static GCSConnection getInstance() {
        return GCSConnection.GSCConnectionHolder.INSTANCE;
    }

    private static class GSCConnectionHolder {
        private static final GCSConnection INSTANCE = new GCSConnection();
    }

    private ExecutorService executorService;

    private GCSConnection() {
        try {
            logger.info("init GCSConnection start...=>>> ");
            executorService = Executors.newFixedThreadPool(5);
            logger.info("init GCSConnection executorService start...=>>> ");
            String CREDENTIAL_FILE = AirinuApiConfig.getConfig().getProperty("CREDENTIAL_FILE", "production-airinu-97e0c8b47ac5.json", "SETTINGS");
            credentials = GoogleCredentials.fromStream(new FileInputStream(BASE_CREDENTIAL_PATH + CREDENTIAL_FILE))
                    .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
            logger.info("init GCSConnection credentials start...=>>> ");
        } catch (IOException e) {
            logger.info("init GCSConnection Err...=>>> ", e);
        }
    }

    public String extractFrame(String videoPath, String fileName) throws IOException {
        FFmpeg ffmpeg = new FFmpeg(FFmpeg.DEFAULT_PATH);
        FFprobe ffprobe = new FFprobe(FFmpeg.DEFAULT_PATH);
        String outPut = System.getProperty("user.dir") + File.separator + "images";
        //  ffmpeg -i input.flv -vf fps=1/60 input/screen_shot_%03d.jpeg
        // logger.info("ffmpeg -i " + inputFile + " -c:v libx264 -crf 23 -profile:v high -r 30 -c:a libfaac -q:a 100 -vf \"scale=trunc(iw/2)*2:trunc(ih/2)*2\" -aspect 16:9 " + convertFileInfo.getFullPath());
        FFmpegBuilder builder = new FFmpegBuilder()
                .setInput(videoPath)
                .addOutput(outPut + "/screen_shot_%03d.jpeg")
                .addExtraArgs("-vf", "fps=1/60")
                .setFrames(3)
                .done();
        FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
        executor.createJob(builder).run();
        return outPut + File.separator + "screen_shot_001.jpeg";
    }


    public Future<UploadSuccessInfo> uploadVideo(Path pathFile, String blobName, String bucketName, String contentType) {
        Callable<UploadSuccessInfo> callable = () -> {
            try {

                String CREDENTIAL_FILE = AirinuApiConfig.getConfig().getProperty("CREDENTIAL_FILE", "production-airinu-97e0c8b47ac5.json", "SETTINGS");
                Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
                logger.info("Upload file {} to bucket name => {} ,contentType :{} ", blobName, bucketName, contentType);
                BlobId blobId = BlobId.of(bucketName, blobName);
                BlobInfo blobInfo = BlobInfo
                        .newBuilder(blobId)
                        .setAcl(new ArrayList<>(Arrays.asList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER))))
                        .setContentType(contentType)
                        .build();
                logger.info("Upload start... +>>>");
                FileInputStream inputStream = new FileInputStream(pathFile.toFile());
                // byte[] data = Files.readAllBytes(pathFile);
                Blob blob = storage.create(blobInfo, inputStream);

                URL signedUrl =
                        blob.signUrl(
                                365,
                                TimeUnit.DAYS,
                                Storage.SignUrlOption.signWith(
                                        ServiceAccountCredentials.fromStream(new FileInputStream(BASE_CREDENTIAL_PATH + CREDENTIAL_FILE))));
                UploadSuccessInfo info = new UploadSuccessInfo();

                String imgUrl = extractFrame(pathFile.getParent() + File.separator + pathFile.getFileName(), blobName);
                Path imgPath = Paths.get(imgUrl);
                logger.info("Upload Image start... +>>>");
                blobId = BlobId.of(bucketName, blobName + File.separator + imgPath.getFileName());
                blobInfo = BlobInfo
                        .newBuilder(blobId)
                        .setAcl(new ArrayList<>(Arrays.asList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER))))
                        .setContentType(JPEG_CONTENT_TYPE)
                        .build();

                Blob blobImg = storage.create(blobInfo, Files.readAllBytes(imgPath));
                URL signedImgUrl =
                        blobImg.signUrl(
                                365,
                                TimeUnit.DAYS,
                                Storage.SignUrlOption.signWith(
                                        ServiceAccountCredentials.fromStream(new FileInputStream(BASE_CREDENTIAL_PATH + CREDENTIAL_FILE))));
                new File(imgUrl).delete();
                logger.info("Upload success ... +>>>");
                info.setBlob(blob);
                info.setBlobImg(blobImg);
                info.setMediaUrl(blob.getMediaLink());
                info.setSignUrl(signedUrl.toString());
                info.setSignUrlTrim(trimLink(signedUrl.toString()));
                info.setImgUrl(signedImgUrl.toString());
                logger.info("Upload succes +>>>url : " + blob.getMediaLink());
                logger.info("Upload succes +>>>signedUrl : " + signedUrl);
                logger.info("Upload succes +>>>signedImgUrl : " + signedImgUrl.toString());
                return info;
            } catch (Exception e) {
                logger.error("uploadFile Err ...=>>> ", e);
            }
            return null;
        };
        return executorService.submit(callable);
    }


    public Future<UploadSuccessInfo> uploadImage(Path pathFile, String blobName, String bucketName, String contentType) {
        Callable<UploadSuccessInfo> callable = () -> {
            try {
                String CREDENTIAL_FILE = AirinuApiConfig.getConfig().getProperty("CREDENTIAL_FILE", "production-airinu-97e0c8b47ac5.json", "SETTINGS");
                Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
                logger.info("Upload image {} to bucket name => {} ,contentType :{} ", blobName, bucketName, contentType);
                BlobId blobId = BlobId.of(bucketName, blobName);
                BlobInfo blobInfo = BlobInfo
                        .newBuilder(blobId)
                        .setAcl(new ArrayList<>(Arrays.asList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER))))
                        .setContentType(contentType)
                        .build();
                logger.info("Upload Image start... +>>>");
                byte[] data = Files.readAllBytes(pathFile);
                Blob blob = storage.create(blobInfo, data);

                URL signedUrl =
                        blob.signUrl(
                                365,
                                TimeUnit.DAYS,
                                Storage.SignUrlOption.signWith(
                                        ServiceAccountCredentials.fromStream(new FileInputStream(BASE_CREDENTIAL_PATH + CREDENTIAL_FILE))));
                UploadSuccessInfo info = new UploadSuccessInfo();
                info.setBlobImg(blob);
                info.setImgUrl(signedUrl.toString());
                info.setMediaUrl(blob.getMediaLink());
                logger.info("Upload image succes +>>>url : " + blob.getMediaLink());
                logger.info("Upload image succes +>>>signedUrl : " + signedUrl);
                return info;
            } catch (Exception e) {
                logger.error("upload Image  Err ...=>>> ", e);
            }
            return null;
        };
        return executorService.submit(callable);
    }


    private String trimLink(String url) {
        if (url.contains("?GoogleAccessId")) {
            return url.substring(0, url.indexOf("?GoogleAccessId"));
        } else
            return url;

    }

}

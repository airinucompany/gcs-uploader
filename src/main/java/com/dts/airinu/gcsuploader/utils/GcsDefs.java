package com.dts.airinu.gcsuploader.utils;

/**
 * @author Rin
 */
public class GcsDefs {
    public static final String MIME_TYPE_VIDEO_MP4 = "video/mp4";
    public static final String MIME_TYPE_IMAGE_JPEG = "image/jpeg";
    public static final String MIME_TYPE_IMAGE_PNG = "image/png";
}
